from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('postgres://tagger:tagger@localhost:5432/tagger', convert_unicode=True, echo=False, client_encoding='utf8')
Base = declarative_base()
Base.metadata.reflect(engine)


#from sqlalchemy.orm import relationship, backref


class Tags(Base):
    __table__ = Base.metadata.tables['tags']

    print(__table__.columns)

    def __init__(self, id, category, parent_id, product):
        self.id = id
        self.category = category
        self.parent_id = parent_id
        self.product = product


    def __repr__(self):
        return "<Tag %r>" % (self.category)


if __name__ == '__main__':
    from sqlalchemy.orm import scoped_session, sessionmaker, Query
    db_session = scoped_session(sessionmaker(bind=engine))
    for item in db_session.query(Tags.id, Tags.category):
        pass
        #print item

    tag =  Tags('teste', 0, 'TESTE')
    #print(tag)

    #db_session.add(tag)
    #db_session.commit()   