SET AUTOCOMMIT = ON;

drop table if exists tags;
drop table if exists product;

drop function if exists parents(int);
drop function if exists children(int);
drop function if exists search_relevance(text, text);

create table tags (
    id SERIAL primary key,
    category text not null,
    parent_id int,
    product text not null
);

create table product (
    id SERIAL primary key,
    name text unique not null
);


create function parents(id int) returns TABLE(id int, category text, parent_id int) 
AS
$$
WITH RECURSIVE tblParent AS
(
    SELECT id, category, parent_id
        FROM Tags WHERE id = $1
    UNION ALL
    SELECT Tags.id, Tags.category, Tags.parent_id
        FROM Tags JOIN tblParent ON Tags.Id = tblParent.parent_id
) 
	select  id, quote_literal(category), parent_id from tblParent where id <> $1
$$
LANGUAGE 'sql';
/*OPTION(MAXRECURSION 32)*/



create function children(id int) returns TABLE(id int, category text, parent_id int) 
AS
$$
WITH RECURSIVE tblChild AS
(
    SELECT id, category, parent_id
        FROM Tags WHERE parent_id = $1
    UNION ALL
    SELECT Tags.id, Tags.category, Tags.parent_id
        FROM Tags JOIN tblChild ON Tags.parent_id = tblChild.id            
) 
	select id, quote_literal(category), parent_id from tblChild where id <> $1
$$
LANGUAGE 'sql';


create function search_relevance(query text, product_name text) returns TABLE(id int, category text, parent_id int) 
AS
$$
WITH tblRelevance AS
(
    SELECT id, category, parent_id
        FROM Tags WHERE lower(category) like lower($1) || '%' and product = product_name
    UNION ALL (
            SELECT id, category, parent_id
                FROM Tags WHERE lower(category) like '%' || lower($1) || '%' and product = product_name
            EXCEPT
            SELECT id, category, parent_id
            FROM Tags WHERE lower(category) like lower($1) || '%' and product = product_name
            )
            
) 
    select id, quote_literal(category), parent_id from tblRelevance where query = $1 and product_name = $2
$$
LANGUAGE 'sql';
/*OPTION(MAXRECURSION 32)*/
grant all on database tagger to tagger; 
grant all on table tags to tagger; 
grant all on sequence tags_id_seq to tagger;    

