#!/bin/sh

exec 1>> ./logs/output.log

ps ax | grep -E "(celery)" | grep -v 'grep' | awk '{print $1}' | xargs kill -9
ps ax | grep -E "(tagger)" | grep -v 'grep' | awk '{print $1}' | xargs kill -9
sleep 1


IP=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')

export CELERY_RESULT_BACKEND="amqp://guest:guest@$1:5672//"
export CELERY_BROKER_URL="amqp://guest:guest@$1:5672//"
export TAGGER_ENV=$2

celery worker -A tagger.celery --loglevel=info &
  
uwsgi --http $IP:8089 --wsgi-file /opt/app/runserver.py --callable app --processes 4 --threads 2 --master --lazy --lazy-apps 2>&1
#uwsgi --http $IP:8089 --wsgi-file runserver.py --callable app

#sudo docker run -it -p 5000:5000 tagger resources/scripts/wsgi_start.sh 10.100.227.156 Production
