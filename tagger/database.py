__author__ = 'david'
"""
Created on 13/10/2015

@author David Pinheiro
    

Module responsible for provinding general database features.
"""

import os

from celery.signals import task_prerun
from contextlib import closing
from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from tagger import app


# UTIL DB METHODS
def init_engine(uri, **kwargs):

    global engine
    global db_session    
    global metadata
    global base

    engine = create_engine(uri, pool_size=5, convert_unicode=True, **kwargs)
    metadata = MetaData()
    db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

    base = declarative_base()
    base.query = base.metadata.reflect(engine)

    return engine


def get_session():
    return db_session


@app.teardown_appcontext
def close_connection(exception):
    """Close the database connection when at request's end"""

    get_session().remove()


@task_prerun.connect
def on_task_init(*args, **kwargs):
    """process signal for handling database problems whithin celery tasks"""

    engine.dispose()


def init_db():
    schema = 'schema.sql'
    if os.environ['TAGGER_ENV'] == 'Testing':
        schema = 'schema_test.sql'

    with closing(get_session()) as session:
        with app.open_resource('../resources/scripts/' + schema, mode='r') as f:
            session.execute(f.read())

        session.commit()

    #Base = declarative_base()
    #query = Base.metadata.reflect(engine)



def query_db(query, args=(), one=False):
    """Executes a provided query
    
    @param query: Query String
    @param args: Query parameters
    @param one: Wether you want fetchone or fetchall
    """

    session = get_session()
    session.execute("SET CLIENT_ENCODING to 'UNICODE'")

    rv = session.execute(query, args)

    return (rv.fetchone() if rv else None) if one else rv
