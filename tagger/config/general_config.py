__author__ = 'david'
"""
Created on 22/09/2015

@author David Pinheiro

@summary:
    - Config
        - ProductionConfig
        - DevelopmentConfig
        - TestingConfig

Module responsible for provinding configuration details according to
especific Enviroment Types such as Production, Testing and Development.     
"""


class Config(object):
    DB_URI = 'postgres://tagger:tagger@142.4.215.94:5432/tagger'        
    SQL_ALCHEMY_ECHO = False
    ALLOWED_EXTENSIONS = set(['xls', 'xlsx'])

    SECRET_KEY = '\xae\xdc\xa0\xb6\xbf\x843\xe5EELd\x99\x07Tt\x92\x16\xa5\xddj\xf0@\xe8' 
    USERNAME = 'admin'
    PASSWORD = 'admin'

    NEW_RELIC_INI_PATH = 'resources/newrelic.ini'
    
    DEBUG = True
    TESTING = False
    TAGGER_IP = "127.0.0.1"


    LANGUAGES = {
        'en': 'English',
        'pt': 'Portuguese'
    }

    @staticmethod
    def factory(type):
        """Factory method for handling Config's subclasses creation

        Classes are wrapped inside method for preventing them to be
        directly instanciated. Re-assign desired variables that 
        should assume different values inside each subclass.

        @param type: subclass name 

        @raise TypeError: When provinding a non existent subclass name 
        """

        type = type + 'Config'

        class ProductionConfig(Config):
            TAGGER_IP = "142.4.215.94"


        class DevelopmentConfig(Config):
            DB_URI = 'postgres://tagger:tagger@142.4.215.94:5432/tagger'
            DEBUG = True
            SQL_ALCHEMY_ECHO = False
            


        class TestingConfig(Config):
            DB_URI = 'postgres://tagger:tagger@142.4.215.94:5432/tagger_test'
            DEBUG = True
            TESTING = True
            SQL_ALCHEMY_ECHO = False
            

        subclasses = Config.__subclasses__()
        types = [subclass.__name__ for subclass in subclasses]

        if type not in types:
            raise TypeError('Invalid Enviroment Type. Possible values : ProductionConfig, DevelopmentConfig, TestingConfig')
        else:
            return eval(type + '()')
