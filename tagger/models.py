# coding: utf-8
__author__ = 'david'

"""
Created on 10/10/2015

@author David Pinheiro

@summary:
    - Tag
    - Product


Module responsible for declaring and mapping our models in our Database
Schema.     
"""

from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.orm import mapper

from tagger.database import metadata, db_session, base



class Tags(object):
    __table__ = base.metadata.tables['tags']

    query = db_session.query_property()

    def __init__(self, id, category, parent_id, product):
        self.id = id
        self.category = category
        self.parent_id = parent_id
        self.product = product

    def __repr__(self):
        return "<Tag %r>" % (self.category)

    def as_dict(self):
       return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Product(object):
    __table__ = base.metadata.tables['product']
    query = db_session.query_property()

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Product %r>" % (self.name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


tags = Table(
    'tags', metadata,
    Column('id', Integer, primary_key=True),
    Column('category', String(50), unique=False),
    Column('parent_id', Integer),
    Column('product', String(20), unique=False)
)

products = Table(
    'product', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(50), unique=True)
)

mapper(Tags, tags)
mapper(Product, products)
