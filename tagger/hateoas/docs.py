__author__ = 'david'

"""
Created on 13/11/2015

@author David Pinheiro

@summary:
    - Doc
        - Resource
    

Module responsible for provinding hateoas features according to
its Resource.
"""

import inspect
import json

from flask import jsonify, request
from representor import Representor


representor = Representor()


class Doc(object):
    """
    This class is intended to hold and dinamically discover setup documentation 
    data and expose an api root endpoint for documentation.
    """

    resources = {}
    conf = {}    
    base_url = ''
    curies = []
    curies_links = []


    def __init__(self, *args): 
        global app
        app = args[0]
        self.conf['docs_url'] = args[1]
        self.representor = None

        Doc.base_url =  ''.join(['/', app.name, self.conf['docs_url'][0:-5]])
        app.add_url_rule(''.join(['/', app.name, '/api']), 'api_root', self.api_root)



    def api_root(self):
        """Api root endpoint"""

        Doc.generate_curies(request.url)

        if self.representor is None:
            self.representor = Representor() 
            self.representor.links.add('self', '/' + app.name + '/api')

            for link in Doc.curies_links:
                (self.representor.meta.links.add(K, V)
                    for K, V in link.iteritems()
                ).next()

        return Doc.get_representor_json(self.representor)



    @staticmethod
    def get_representor_json(representor, custom_values=[]):
        """handle hal translation for suporting custom curie format

        @param representor: The representor object you want to customize
        @param custom_values: A list of customized links to be added
        """

        hal_translation = representor.translate_to('application/hal+json')
        hal_dict = json.loads(hal_translation)
        hal_dict['_links']['curies'] = Doc.curies

        [hal_dict['_links'].update(element) for element in custom_values]
        return jsonify(hal_dict)



    @staticmethod
    def generate_curies(request_url):
        """Generate the basic curie items(templated and it's rels)

        @param request_url: the url from flask's request.
        """

        if len(Doc.curies) == 0:
            end_url_position  = request_url[8:].find('/') + 9
            base_url = request_url[:end_url_position] + app.name
            Doc.curies.append({
                'name': 'doc',
                'href': '{}{}'.format(base_url, Doc.conf['docs_url']),
                'templated': True
            })

            ([Doc.curies_links.append({'doc:' + resource: '/' + resource})
                for resource in Doc.resources])



    class Resource(object):
        """
        This class is intended to be used as a decorator and help discover view 
        Resources and their endpoints, thus describing them on our Doc.
        """

        def __init__(self, *args):
            self.resource = args[0]
            self.hal = None


        def __call__(self, cls):
            Doc.resources[self.resource] = cls
            self.urls = {url.endpoint: url.rule for url in app.url_map.iter_rules()}

            new_url = '{}{}'.format(Doc.base_url, self.resource)
            app.add_url_rule(new_url, self.resource + '_doc', self.resource_doc, methods=['GET'])


        def resource_doc(self):
            """Endpoint for resource's documentation"""

            if self.hal is None:
                Doc.generate_curies(request.url)
                representor = Representor()

                slash_position  = request.path.rfind('/')
                resource_name = request.path[slash_position+1:]

                representor.links.add('self', request.path)
                resource_name = Doc.resources[resource_name]
                example_links = self.get_translated_resource_endpoints(resource_name)

                for link in Doc.curies_links:
                    (representor.meta.links.add(K, V)
                        for K, V in link.iteritems()
                    ).next()


                self.hal = Doc.get_representor_json(
                    representor, custom_values=example_links
                )

            return self.hal



        def get_translated_resource_endpoints(self, resource_name):
            """handle resource's endpoints adding support for templated

            checks wether inspected endpoint has parameters to be
            exemplified in the docs.

            @param resource_name: resource's name
            """

            example_links = []
            for name, m in inspect.getmembers(resource_name, inspect.ismethod):
                example = self.get_example_from_docstring(m)

                link = {name: {
                        'href': self.urls[name] + example}
                }

                if example is not None:
                    link[name]['templated'] = True

                example_links.append(link)

            return example_links



        def get_example_from_docstring(self, func):
            """search resource's endpoints for information in docstrings

            Inspect docstring for @example notation. This element must
            provide an templated example endpoint, showing all the
            required parameters.

            @param func: function to be inspected 
            """

            docstring = func.__doc__.split('\n')
            example = None

            for line in docstring:
                if line.strip().startswith('@example:'):
                    example = line.strip()[10:]
                    break

            return example
