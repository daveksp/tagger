__author__ = 'david'

"""
Created on 28/09/2015

@author David Pinheiro

@summary:
    - Tags    
    - Related
    - Taxonomy
    - TaxonomyUpload
    - Product
    
    

Module responsible for provinding rest methods according to
its Resource.     
"""

import ast
import uuid

from flask import jsonify, request, render_template, url_for
from flask.ext.babel import Babel, gettext
from openpyxl import load_workbook

from tagger import app, celery, doc
from tagger.logger.log import create_logger, log
from tagger.repository.TagRepository import load_tags_by_id, search_tag_by_name, search_related
from tagger import service

logger = create_logger(__name__)


def create_response_base(uuid_value):
    if uuid_value is None:
        uuid_value = uuid.uuid4()
    return dict(result=[], status_code=500, uuid=uuid_value, message='')


@doc.Resource('tags')
class Tags(object):


    @app.route('/tagger/api/v2/tags', methods=['GET'])
    def search_child_by_name():
        """search child by name

        @example: ?name={name}&product={product}&begin={begin}&limit={limit}
        """

        response = create_response_base(request.args.get('uuid'))
        log(logger, response['uuid'], 'starting request', params=request.args)
        
        name = request.args.get('name')
        product = request.args.get('product')
        begin = request.args.get('begin')
        limit = request.args.get('limit', 10)
        level = 'error'

        if product is None or product.strip() == '':
            response['message'] = gettext('pproduct_required')
        elif begin != 'true' and begin != 'false':
            response['message'] = gettext('invalid_begin_value')
        else:
            result = search_tag_by_name(name, product, begin, limit)
            response['result'] = result
            level = 'info'
            response['status_code'] = 200

        log(logger, response['uuid'], response, level=level)
        return jsonify(response), response['status_code']


@doc.Resource('related')
class Related(object):


    @app.route('/tagger/api/v2/related', methods=['GET'])
    def search_related():   
        """search related

        @example: ?tag_id={tag_id}&relationship={relationship}&limit={limit}
        """

        response = create_response_base(request.args.get('uuid'))
        log(logger, response['uuid'], 'starting request', params=request.args)
        
        tag_id = request.args.get('tag_id')
        relationship = request.args.get('relationship')
        limit = request.args.get('limit', 10)
        level = 'error'

        if tag_id is not None and not tag_id.isdigit():
            response['message'] = gettext('wrong_id_type')
        elif relationship != 'parents' and relationship != 'children':
            response['message'] = gettext('invalid_relationship_value')
        else:
            result = search_related(tag_id, relationship, limit)
            response['result'] = result
            level = 'info'
            response['status_code'] = 200

        log(logger, response['uuid'], response, level=level)
        return jsonify(response), response['status_code']


@doc.Resource('taxonomy')
class Taxonomy(object):


    @app.route('/tagger/api/v2/taxonomy', methods=['GET'])
    def load_tags_by_id():
        """load tags by id

        @example: ?ids={ids}
        """

        response = create_response_base(request.args.get('uuid'))
        log(logger, response['uuid'], 'starting request', params=request.args)

        ids = request.args.get('ids')
        level = 'error'

        try:
            ids = ast.literal_eval(ids)

            if type(ids) != list:
                response['message'] = gettext('wrong_ids_type')
            elif not ids:
                response['message'] = gettext('empty_id_list')
            else:
                ids = tuple(ids)

                result = load_tags_by_id(ids)
                response['result'] = result
                level = 'info'
                response['status_code'] = 200

        except ValueError as ex:
            response['message'] = gettext('wrong_ids_type')

        log(logger, response['uuid'], response, level=level)
        return jsonify(response), response['status_code']


@doc.Resource('taxonomy_upload')
class TaxonomyUpload(object):


    @app.route('/tagger/api/v2/taxonomy_upload', methods=['PUT'])
    def upload_taxonomy_file():
        """upload taxonomy file

        @example: ?product_type={product_type}
        """

        response = create_response_base(request.args.get('uuid'))
        log(logger, response['uuid'], 'starting request', params=request.args)
        level = 'info'

        product = request.form.get('product_type')
        spreedsheet = request.files.get('spreedsheet')
        taxonomy = []
        ws = None
        wb = load_workbook(filename=spreedsheet, read_only=True)
        sheet_names = wb.get_sheet_names()

        for name in sheet_names:
            ws = wb[name]
        
            map(
                lambda row: taxonomy.append(row[0].value.encode('utf8')),
                filter(
                    lambda row: True if row[0].value and isinstance(row[0].value, basestring) else False, ws.rows)
            )  

        task = service.handle_taxonomy.apply_async(args=[taxonomy, product])
        response['message'] = gettext('successfully_imported_spreadsheet')

        log(logger, response['uuid'], response, level=level)
        return jsonify(response), response['status_code'], {
            'location': url_for('upload_status') + '?task_id=' + task.id}


    @app.route('/tagger/api/v2/taxonomy_upload', methods=['GET'])
    def  upload_status():
        """upload status

        @example: ?task_id={task_id}
        """

        response = create_response_base(request.args.get('uuid')) 
        log(logger, response['uuid'], 'starting request', params=request.args)
        level = 'info'
        response['status_code'] = 200

        task_id = request.args.get('task_id')
        task = service.handle_taxonomy.AsyncResult(task_id)

        if task.info is not None:
            result = dict(status=task.state, current=task.info.get('current'),
                          total=task.info.get('total'), progress=task.info.get('progress'),
                          message=task.info.get('msg'))
        else:
            result = dict(status='PENDING', current=0,
                          total=0, progress=0,
                          message='')

        response['result'] = result
        log(logger, response['uuid'], response, level=level)
        return jsonify(response), response['status_code']


#@doc.Resource('product')
class Product(object):


    @app.route('/tagger/api/v2/product', methods=['PUT'])
    def create_product():
        pass

    @app.route('/tagger/api/v2/product', methods=['POST'])
    def update_product():
        pass

    @app.route('/tagger/api/v2/product', methods=['GET'])
    def find():
        pass


@app.route('/tagger')
def init_page():
    return render_template('index.html')
    

@app.errorhandler(404)
def not_found(error):
    """handler for not_found error"""

    response = create_response_base(request.args.get('uuid')) 
    log(logger, response['uuid'], 'starting request', params=request.args)

    response['status_code'] = 404
    response['message'] = gettext('not_found')
    log(logger, response['uuid'], response, level='error')
    return jsonify(response), response['status_code']
