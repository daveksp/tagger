__author__ = 'david'

"""
Created on 13/10/2015

@author David Pinheiro

Module responsible for provinding access to methods that will be
executed as celery tasks. Flask doesn't support parallel process
so celery is the responsible for handling these requests in a
non-blocking way.
"""

import celery

from tagger.repository.TagRepository import save_tag, search_tagid_by_name


@celery.task(bind=True)
def handle_taxonomy(self, taxonomy, product):
    """Method for handle a list of taxonomy tags

    Method call by init_db method. It'll
    check for tag existence in database and save it aconrdingly to
    it's parent tag and product. 

    @param taxonomy: A list of tag strings separated by '/' indicating
                     the relationship between tags.

                     e.g: geography/cold war/capitalism
                          geography/cold war/socialism

                     this taxonomy representation will generate 4 inserts
                     in the database with this inheritance:
                    - geography
                        - cold war
                            - capitalism
                            - socialism


    @param product: The product owner of tags.
    """

    taxonomy_size = len(taxonomy)
    message = ''
    for i, description in enumerate(taxonomy):
        categories = description.split('/')
        categories.insert(0, '')
        pairs = [[categories[x+1], categories[x+2]] for x in xrange(-1, len(categories)-2)]

        map(
            lambda x: save_tag(x[1], product, search_tagid_by_name(x[0], product)),
            filter(lambda x: True if search_tagid_by_name(x[1], product) == 0 else False, pairs)
        )

        if i % 50 == 0:           
            progress = (i * 100) / taxonomy_size
            message = '{0} de {1} terms imported'.format(i, taxonomy_size)

            self.update_state(state='PROGRESS', 
                              meta={
                                       'current': i, 'total': taxonomy_size,
                                       'progress': progress, 'msg': message
                             })

    return {'current': taxonomy_size, 'total': taxonomy_size, 'progress': 100}
