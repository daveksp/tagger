__author__ = 'david'
"""
Created on 13/10/2015

@author David Pinheiro


Module responsible for provinding database features for Tags Resource.
The decision for exposing methods this way is for promove code reusabilty
and also keep some complex database code centered in only one place. It's 
better for maintenance.
"""

import ast

from tagger.database import db_session, query_db, get_session
from tagger.models import Tags


def save_tag(category, product, parent_id=0):
    if category != '':
        tag = Tags(None, category, parent_id, product)
        get_session().add(tag)
        get_session().commit()


def search_tagid_by_name(category, product):
    tag = Tags.query.filter(Tags.category == category, Tags.product == product).first()

    return tag.id if tag is not None else 0


def search_tag_by_name(name, product, begin, limit):
    """Method responsible for search child by name

    @param nome: The tag's name
    @param product: Tag Owner Product
    @param begin: Wether we want to look for tags who starts with
                  provided name, or weher name is place anwhere.
    @param limit: Tag's result limit (default 10)
    """

    result = []

    if begin == 'true':
        query_filter = '{0}{1}'.format(name.encode('utf8'), '%')
        
        items = Tags.query.filter(
            Tags.category.ilike(query_filter), Tags.product == product
        ).limit(limit).all()    

        result.extend([item.as_dict() for item in items])
    else:
        query = 'select search_relevance(:text, :product_name) limit :limit' 
        tags = query_db(query, {'text': name, 'product_name': product, 'limit': limit})

        for row in tags:                  
            items = map(lambda x: ast.literal_eval(x), [row[0]])

            (result.extend([Tags(item[0], item[1].translate(None, "'"), item[2], None)
                .as_dict() for item in items]))

    return result



def load_tags_by_id(ids):
    """Method responsible for search child by ids
    
    @param ids: Id list 
    """

    result = Tags.query.filter(Tags.id.in_(ids))
    result = [tag.as_dict() for tag in result]
    return result



def search_related(tag_id, relationship, limit):
    """Method responsible for related tags acording to desired relationship

    @param tag_id: The tag's id
    @param relationship: Desired relationship
    @param limit: Tag's result limit (default 10)
    """

    query = 'select ' + relationship + '(:tag_id) limit :limit'

    tags = query_db(query, {'tag_id': tag_id, 'limit': limit})
    result = []
    for row in tags:
        items = map(lambda x: ast.literal_eval(x), [row[0]])
        (result.extend([Tags(item[0], item[1], item[2], None)
            .as_dict() for item in items]))

    return result
