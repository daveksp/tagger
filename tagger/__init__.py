# This Python file uses the following encoding: utf-8
import os

from celery import Celery
from flask import Flask, request
from flask_cors import CORS
from flask.ext.babel import Babel
from newrelic import agent

from config.general_config import Config
from tagger.hateoas.docs import Doc


app = Flask(__name__)
babel = Babel(app, default_locale='en')

celery = Celery(
    app.name,
    backend=os.environ['CELERY_RESULT_BACKEND'],
    broker=os.environ['CELERY_BROKER_URL']
)

doc = Doc(app, '/api/v2/docs/{rel}')


try:
    enviroment = os.environ['TAGGER_ENV']
except KeyError as ex:
    enviroment = 'Testing'

config = Config.factory(enviroment)
app.config.from_object(config)


from tagger.database import init_engine, init_db
# iNIT SQLALCHEMY ENGINE
init_engine(app.config['DB_URI'], echo=app.config['SQL_ALCHEMY_ECHO'], client_encoding='utf8')



if enviroment == 'Testing':
    init_db()

import tagger.views


# CONFIG CROSS ORIGIN REQUEST SHARING
CORS(app, resources=r'/*', allow_headers='Content-Type', supports_credentials=True)



# STARTS NEW RELIC
if enviroment != 'Testing':
    envs = {'Production': 'development', 'Testing': 'development', 'Development': 'development'}
    agent.initialize(app.config['NEW_RELIC_INI_PATH'], envs[enviroment])    



@babel.localeselector
def get_locale():
    return request.accept_languages.best_match(app.config['LANGUAGES'].keys())
