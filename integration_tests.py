# coding: utf-8
__author__ = 'david'

import json
import os
import pdb
import sys
import time
import unittest

from flask.ext.babel import Babel, gettext
from mock import patch, Mock, MagicMock
from openpyxl import load_workbook
from sqlalchemy.orm import sessionmaker
from StringIO import StringIO

from selenium import webdriver

os.environ['TAGGER_ENV'] = 'Testing'
os.environ['CELERY_RESULT_BACKEND'] = 'amqp://guest:guest@142.4.215.94:5672//'
os.environ['CELERY_BROKER_URL'] = 'amqp://guest:guest@142.4.215.94:5672//'

import tagger
from tagger.config.general_config import Config         
from tagger.database import get_session, init_db, init_engine, engine
from tagger.repository.TagRepository import save_tag
from tagger.service import handle_taxonomy

reload(sys)
sys.setdefaultencoding("utf-8")

class manageTestCase(unittest.TestCase):  
    
    connection = None
    transaction = None
    

    def setUp(self):
        self.app = tagger.app.test_client()     
        self.init_db()                
        
                
    def tearDown(self):
        pass        
            
   
    #  UNIT TEST FOR TAGS METHODS
    def test_tags_get_begin(self):                        
        rv = self.app.get('/tagger/api/v2/tags?name=ocea&begin=true&product=LOS')   
        tags = json.loads(rv.data)
        
        assert len(tags['result']) == 2        
        assert tags['result'][0]['category'] == 'oceano'
        assert tags['result'][1]['category'].decode('utf-8') == 'oceanográfia'.decode('utf-8')
        


    def test_tags_get_case_insensitive(self):                        
        rv = self.app.get('/tagger/api/v2/tags?name=OCEA&begin=true&product=LOS')   
        tags = json.loads(rv.data)
        
        assert len(tags['result']) == 2        
        assert tags['result'][0]['category'] == 'oceano'
        assert tags['result'][1]['category'].decode('utf-8') == 'oceanográfia'.decode('utf-8')
        

    
    def test_tags_get_anywhere(self):        
        rv = self.app.get('/tagger/api/v2/tags?name=gráfia&begin=false&product=LOS')
        tags = json.loads(rv.data)
        
        assert len(tags['result']) == 2
        assert tags['result'][0]['category'].decode('utf-8') == 'geográfia'.decode('utf-8')
        assert tags['result'][1]['category'].decode('utf-8') == 'oceanográfia'.decode('utf-8')
        

    def test_tags_get_limit(self):
        
        
        rv = self.app.get('/tagger/api/v2/tags?name=gráfia&begin=false&limit=1&product=LOS')
        tags = json.loads(rv.data)
        assert len(tags['result']) == 1
        assert tags['result'][0]['category'].decode('utf-8') == 'geográfia'.decode('utf-8')
        


    def test_tags_get_begin_no_match(self):        
        rv = self.app.get('/tagger/api/v2/tags?name=terac&begin=true&product=LOS')
        tags = json.loads(rv.data)
        assert len(tags['result']) == 0
        
        tags = None

    

    def test_tags_get_anywhere_no_match(self):
        rv = self.app.get('/tagger/api/v2/tags?name=terac&begin=false&product=LOS')
        tags = json.loads(rv.data)
        assert len(tags['result']) == 0



    def test_tags_get_begin_no_filter(self):
        rv = self.app.get('/tagger/api/v2/tags?name=&begin=true&product=LOS')        
        tags = json.loads(rv.data)
        assert len(tags['result']) == 5



    def test_tags_get_anywhere_no_filter(self):
        rv = self.app.get('/tagger/api/v2/tags?name=&begin=false&product=LOS')        
        tags = json.loads(rv.data)
        assert len(tags['result']) == 5


    
    def test_tags_get_no_begin(self):
        rv = self.app.get('/tagger/api/v2/tags?name=2&product=LOS')        
        tags = json.loads(rv.data)        
        assert tags['message'] == 'Not a valid value for begin parameter. Possible values : True, False'
        assert tags['status_code'] == 500


    def test_tags_get_no_product(self):
        rv = self.app.get('/tagger/api/v2/tags?name=terac&begin=true')
        tags = json.loads(rv.data)        
        assert tags['message'] == gettext('pproduct_required')
        assert tags['status_code'] == 500

    #CASO DE teste sem product
    

    #  UNIT TEST FOR RELATED METHODS
    def test_related_get_children(self):        
        rv = self.app.get('/tagger/api/v2/related?tag_id=2&relationship=children')
        tags = json.loads(rv.data)

        assert len(tags['result']) == 3
        assert tags['result'][0]['category'] == 'oceano'
        assert tags['result'][1]['category'].decode('utf-8') == 'oceanográfia'.decode('utf-8')



    def test_related_get_parents(self):        
        rv = self.app.get('/tagger/api/v2/related?tag_id=3&relationship=parents')
        tags = json.loads(rv.data)
        
        assert len(tags['result']) == 2
        assert tags['result'][0]['category'] == 'agua'
        assert tags['result'][1]['category'].decode('utf-8') == 'geográfia'.decode('utf-8')



    def test_related_get_children_limit(self):        
        rv = self.app.get('/tagger/api/v2/related?tag_id=2&relationship=children&limit=1')
        tags = json.loads(rv.data)

        assert len(tags['result']) == 1
        assert tags['result'][0]['category'] == 'oceano'        



    def test_related_get_parents_limit(self):        
        rv = self.app.get('/tagger/api/v2/related?tag_id=3&relationship=parents&limit=1')
        tags = json.loads(rv.data)
        
        assert len(tags['result']) == 1
        assert tags['result'][0]['category'] == 'agua'        



    def test_related_get_no_child(self):
        rv = self.app.get('/tagger/api/v2/related?tag_id=9999999&relationship=children')
        tags = json.loads(rv.data)
        assert len(tags['result']) == 0



    def test_related_get_no_parents(self):
        rv = self.app.get('/tagger/api/v2/related?tag_id=9999999&relationship=parents')
        tags = json.loads(rv.data)
        assert len(tags['result']) == 0



    def test_related_get_child_no_tagid(self):
        rv = self.app.get('/tagger/api/v2/related?relationship=children')
        tags = json.loads(rv.data)        
        assert len(tags['result']) == 0



    def test_related_get_parents_no_tagid(self):
        rv = self.app.get('/tagger/api/v2/related?relationship=parents')
        tags = json.loads(rv.data)        
        assert len(tags['result']) == 0



    def test_related_get_no_relationship(self):
        rv = self.app.get('/tagger/api/v2/related?tag_id=2')
        tags = json.loads(rv.data)                
        assert tags['message'] == 'Not a valid value for relationship parameter. Possible values : parents, children'
        assert tags['status_code'] == 500


    
    def test_related_get__children_wrong_argument(self):
        rv = self.app.get('/tagger/api/v2/related?tag_id=a&relationship=children')
        tags = json.loads(rv.data)
        assert tags['message'] == gettext('wrong_id_type')
        assert tags['status_code'] == 500


      
    def test_related_get_parents_wrong_argument(self):
        rv = self.app.get('/tagger/api/v2/related?tag_id=a&relationship=parents')
        tags = json.loads(rv.data)        
        assert tags['message'] == gettext('wrong_id_type')




    #  UNIT TEST FOR TAXONOMY METHODS
    def test_taxonomy_get(self):        
        rv = self.app.get('/tagger/api/v2/taxonomy?ids=[1,2,3]')
        tags = json.loads(rv.data)
        #print(tags)
        
        assert len(tags['result']) == 3
        assert tags['result'][0]['category'].decode('utf-8') == 'geográfia'.decode('utf-8')
        assert tags['result'][1]['category'] == 'agua'
        assert tags['result'][2]['category'] == 'oceano'
        


    def test_taxonomy_get_empty_ids(self):        
        rv = self.app.get('/tagger/api/v2/taxonomy?ids=[]')
        tags = json.loads(rv.data)        
        assert tags['message'] == gettext('empty_id_list')
        assert tags['status_code'] == 500
        


    def test_taxonomy_get_not_list(self):        
        rv = self.app.get('/tagger/api/v2/taxonomy?ids=a')
        tags = json.loads(rv.data)        
        assert tags['message'] == gettext('wrong_ids_type')
        assert tags['status_code'] == 500



    def test_taxonomy_get_string_in_list(self):        
        rv = self.app.get('/tagger/api/v2/taxonomy?ids=[1,a]')
        tags = json.loads(rv.data)                
        assert tags['message'] == gettext('wrong_ids_type')
        assert tags['status_code'] == 500




    #  UNIT TEST FOR INDEX and 404Handler    
    def test_index(self):
        """@todo substituir pelo selenium"""
        rv = self.app.get('/tagger')
        assert rv.data.count('<title>Tagger</title>') > 0

    

    def test_not_found_handler(self):
        rv = self.app.get('/tagger/api/v2/pipocas')
        tags = json.loads(rv.data)
        assert tags['message'] == 'url not found'
        assert tags['status_code'] == 404


    
    #  UNIT TEST FOR TAXONOMY UPLOAD METHOD    
    def test_taxonomy_upload_put(self):
        workbooks = ['historia']
        tagger.celery.conf.update(CELERY_ALWAYS_EAGER=True,)
        with patch('tagger.views.load_workbook') as load_workbook_mock:
            wb_mock = MagicMock()
            load_workbook_mock.return_value = wb_mock
            wb_mock.get_sheet_names.return_value = workbooks            
            
            ws_mock = Mock()
            ws_row_mock = Mock()
            ws_mock.rows = [[ws_row_mock]]
            ws_row_mock.value = 'historia/historia do Brasil/a evolucao colonial brasileira'
            wb_mock.__getitem__.return_value = ws_mock

            rv = self.app.put('/tagger/api/v2/taxonomy_upload', 
                              data=dict(product_type='LOS',
                                        spreedsheet=(StringIO('faked spreedsheet'), 'hello world.xls')
                                       ))

            result = json.loads(rv.data)
            time.sleep(4)
                        
            assert result['message'] == 'Spreedsheet successfully imported'
            rv = self.app.get('/tagger/api/v2/tags?name=a evol&begin=true&product=LOS')
            
            tags = json.loads(rv.data)
            
            assert len(tags['result']) == 1
            assert tags['result'][0]['category'] == 'a evolucao colonial brasileira'



    def test_taxonomy_upload_get(self):
        workbooks = ['historia']
        tagger.celery.conf.update(CELERY_ALWAYS_EAGER=True,)
        with patch('tagger.views.load_workbook') as load_workbook_mock:
            wb_mock = MagicMock()
            load_workbook_mock.return_value = wb_mock
            wb_mock.get_sheet_names.return_value = workbooks            
            
            ws_mock = Mock()
            ws_row_mock = Mock()
            ws_mock.rows = [[ws_row_mock]]
            ws_row_mock.value = 'historia/historia do Brasil/a evolucao colonial brasileira'
            wb_mock.__getitem__.return_value = ws_mock

            rv = self.app.put('/tagger/api/v2/taxonomy_upload',
                            data=dict(product_type='LOS',
                                      spreedsheet=(StringIO('faked spreedsheet'), 'hello world.xls')
                             ))

            time.sleep(2)                       
            rv = self.app.get(rv.headers['Location'][17:])
            result = json.loads(rv.data)
            
            assert result['result']['status'] == 'SUCCESS' or result['result']['status'] == 'PROGRESS'




    #  UNIT TEST FOR INTERNAL METHODS
    def test_handle_taxonomy(self):
        tagger.celery.conf.update(CELERY_ALWAYS_EAGER=True,)
        taxonomy = self.create_taxonomy()
        with tagger.app.test_request_context('/test'):
            task = handle_taxonomy.delay(taxonomy, 'LOS')                   
        
        time.sleep(2)
        rv = self.app.get('/tagger/api/v2/related?tag_id=7&relationship=parents')        
        tags = json.loads(rv.data)                
                
        assert len(tags['result']) == 2
        assert tags['result'][0]['id'] == 6
        assert tags['result'][0]['category'] == "'a ordem bipolar'"
        assert tags['result'][0]['parent_id'] == 1


                  
    def test_config_wrong_type(self):
        with self.assertRaises(TypeError):
            config = Config.factory('NonExistentType')
            app.config.from_object(config)    




    def create_taxonomy(self):
        # Important to keep geográfia or use a term for validating UTF-8 Encoding   

        taxonomy = []
        taxonomy.append('geográfia')
        taxonomy.append('geográfia/a ordem bipolar')
        taxonomy.append('geográfia/a ordem bipolar/capitalismo')
        taxonomy.append('geográfia/a ordem bipolar/capitalismo x socialismo')

        return taxonomy

    
    def init_db(self):        
        init_db()

        tags = [['geográfia', 0],['agua', 1], ['oceano', 2],
                ['oceanográfia', 2], ['abissais', 3]]
        

        with tagger.app.test_request_context('/test'):
            map(lambda tag: save_tag(tag[0], 'LOS' ,tag[1]), tags)
        


if __name__ == '__main__':
	unittest.main()